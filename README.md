# Data Structures and Algorithms
A simple repository to explore some data structures and algorithms

## Setup

Install [python 3.9](https://www.python.org/downloads/release/python-390/), I am using [VSCode](https://code.visualstudio.com/) but any IDE is fine.

If on windows, you may need to restart for the PATH to be reloaded so that python works correctly.

## Dependencies

```
pip install pipenv
pipenv install -d
```

If using VSCode, you may want to set the interpreter to the Pipenv python. See [here](https://code.visualstudio.com/docs/python/environments)

## Running

This repository has unit tests that verify the functionality of the code. Initially, all tests should pass.

To work on the code, read the comments, and refactor the code per the instructions.

To run the unit tester constantly:

```
pipenv run ptw
```

unit tests will be automatically re-run when the file is saved.

You can also manually execute tests by running

```
pipenv run pytest
```

## File Order

It is suggested that you start on the code in this order:

1. test_array.py
2. test_array_sorts.py
3. test_searching.py
4. test_sets.py
5. test_stacks.py
6. test_queue.py