import pytest

testdata = [
    ([100, 63, 21, 37, 45, 29, 1, 939, -100, 0, 10000], [-100, 0, 1, 21, 29, 37, 45, 63, 100, 939, 10000]),
    ([], []),
    ([0], [0]),
    ([-1, 1, -2, 2, -3, 3, 0, 0], [-3, -2, -1, 0, 0, 1, 2, 3])
]

def selection_sort(array):
    # https://ja.wikipedia.org/wiki/%E9%81%B8%E6%8A%9E%E3%82%BD%E3%83%BC%E3%83%88
    return array

def insertion_sort(array):
    # https://ja.wikipedia.org/wiki/%E6%8C%BF%E5%85%A5%E3%82%BD%E3%83%BC%E3%83%88
    return array

@pytest.mark.parametrize("input, expected", testdata)
def test_selection_sort_parametrize(input, expected):
    # Delete the next two lines, uncomment the line below when the sort is implemented
    input.sort()
    assert input == expected
    #assert selection_sort(input) == expected

@pytest.mark.parametrize("input, expected", testdata)
def test_insertion_sort_parametrize(input, expected):
    # Delete the next two lines, uncomment the line below when the sort is implemented
    input.sort()
    assert input == expected
    #assert insertion_sort(input) == expected

class ComplexObject:
    def __init__(self, number, name):
        self.number = number # A number
        self.name = name # A string
    def __eq__(self, other):
        if not isinstance(other, ComplexObject):
            return NotImplemented
        return self.number == other.number and self.name == other.name

def test_sort_objects():
    # This problem requires sorting objects based on multiple attributes.
    # Requirements: Sorting for these objects should first consider the number.
    # If the number is equal, then sort based on the name.
    array = [ComplexObject(2, "Bananas"), ComplexObject(3, "Cherries"), ComplexObject(4, "Dates"), ComplexObject(3, "Green"),
     ComplexObject(1, "Red"), ComplexObject(2, "Blue"), ComplexObject(4, "Yellow"), ComplexObject(1, "Apples")]

    expected_array = [ComplexObject(1, "Apples"), ComplexObject(1, "Red"), ComplexObject(2, "Bananas"), ComplexObject(2, "Blue"), 
        ComplexObject(3, "Cherries"), ComplexObject(3, "Green"), ComplexObject(4, "Dates"), ComplexObject(4, "Yellow")]

    def selection_sort(array, compare_function):
        # Implement a selection sort that takes a comparison function for the object
        return array

    def object_comparator(co1, co2):
        #this function should return 0 for equal items
        # -1 for co1 < co2
        # and +1 for co1 > co2
        return 0

    selection_sort(array, object_comparator)

    for i in range(len(array)):
        print(array[i].number, array[i].name)
        print(expected_array[i].number, expected_array[i].name)
        # Uncomment the line below, and delete the last line
        #assert array[i] == expected_array[i]
        assert expected_array[i] == expected_array[i]