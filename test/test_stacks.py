class Stack:
    # Limitation: do not use len(data)
    def __init__(self):
        self.head = 0
        self.data = []

    def push(self, element):
        # Replace data append with a custom implementation that uses self.head to track the top of the queue
        self.data.append(element)

    def pop(self):
        # Replace pop with a custom implementation that uses the self.head to remove the element
        return self.data.pop()


def test_stack():
    # After you implement the stack above, this test should still pass
    stack = Stack()
    stack.push('a')
    stack.push('b')
    stack.push('c')
    assert stack.pop() == 'c'
    stack.push('3')
    stack.push('4')
    assert stack.pop() == '4'
    assert stack.pop() == '3'
    assert stack.pop() == 'b'
    assert stack.pop() == 'a'


def test_flatten_array():
    array = [[5], [4, 3], [2], [[[1]]]]

    # Implement flatten using a stack, instead of using recursion (no recursion allowed)
    # This is a very difficult problem
    flat_list = [5, 4, 3, 2, 1]

    assert flat_list == [5, 4, 3, 2, 1]