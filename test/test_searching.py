
import pytest

testdata = [
    ([-10, -5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 34, 99, 100, 200, 10000], 5, True),
    ([-10, -5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 34, 99, 100, 200, 10000], -100, False),
    ([-10, -5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 34, 99, 100, 200, 10000], 10000, True),
    ([-10, -5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 34, 99, 100, 200, 10000], 0, False)]


@pytest.mark.parametrize("array, value, expected", testdata)
def test_binary_search_recursive(array, value, expected):
    # Replace with implementation of binary search using recursion
    assert (value in array) == expected
    

@pytest.mark.parametrize("array, value, expected", testdata)
def test_binary_search_non_recursive(array, value, expected):
    # Replace with implementation of binary search
    assert (value in array) == expected
    