class Queue:
    def __init__(self, capacity):
        self.head = 0
        self.tail = 0
        self.capacity = capacity
        self.data = []

    def enqueue(self, element):
        # Replace data append with a custom implementation that uses head to track the head
        self.data.append(element)

    def dequeue(self):
        # Replace pop with a custom implementation that uses the head to remove the element
        return self.data.pop()


def test_queue():
    pass
    # Uncomment this code to test the implementation of Queue
    # stack = Queue(4)
    # stack.enqueue('a')
    # stack.enqueue('b')
    # stack.enqueue('c')
    # assert stack.dequeue() == 'a'
    # stack.enqueue('3')
    # stack.enqueue('4')
    # assert stack.dequeue() == 'b'
    # assert stack.dequeue() == 'c'
    # assert stack.dequeue() == '3'
    # assert stack.dequeue() == '4'