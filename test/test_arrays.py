
def test_array_append():
    array = []
    # Delete the line below, and write your own implementation without using a built in function
    # (you can use [] syntax or set) - this is a warm up exercise
    array.append('apples')

    assert array == ['apples']

def test_array_insert():
    array = ['bananas', 'cherries', 'dates']
    # Delete the line below, and write your own implementation without using a built in function
    # You will need to move elements.
    array.insert(0, 'apples')
    array.insert(2, 'figs')

    assert array == ['apples', 'bananas', 'figs', 'cherries', 'dates']

def test_reverse_array():
    array = [100, 63, 21, 37, 45, 29, 1, 939, -100, 0, 10000]
    
    # Delete the line below, and write your own implementation without using a built in function
    array.reverse()

    assert array == [10000, 0, -100, 939, 1, 29, 45, 37, 21, 63, 100]

def test_sort_array():
    array = [100, 63, 21, 37, 45, 29, 1, 939, -100, 0, 10000]

    # Delete the line below, and write your own implementation without using a built in function
    array.sort()

    assert array == [-100, 0, 1, 21, 29, 37, 45, 63, 100, 939, 10000]
    
def test_sort_array_reverse():
    array = [100, 63, 21, 37, 45, 29, 1, 939, -100, 0, 10000]

    # Delete the line below, and write your own implementation without using a built in function
    # Limitation: Do not create a new array, do this using the existing array (difficult!)
    array.sort(reverse=True)

    assert array == [10000, 939, 100, 63, 45, 37, 29, 21, 1, 0, -100]

def test_flatten_array():
    array = [[5], [4, 3], [2], [[[1]]]]

    # Implement a function to generate this array (it is difficult - you can use recursion)
    flat_list = [5, 4, 3, 2, 1]

    assert flat_list == [5, 4, 3, 2, 1]