# https://ja.wikipedia.org/wiki/%E9%9B%86%E5%90%88%E8%AB%96

def test_set_union():
    # https://ja.wikipedia.org/wiki/%E5%92%8C%E9%9B%86%E5%90%88
    set1 = {"apples", "bananas", "cherries", "oranges"}
    set2 = {"pears", "apples", "oranges", "kiwis"}

    # Create set 3 by combining set1 and set2 with union
    set3 = {"apples", "bananas", "cherries", "pears", "oranges", "kiwis"}
    assert set3 == {"apples", "bananas", "cherries", "pears", "oranges", "kiwis"}

def test_set_intersection():
    # https://ja.wikipedia.org/wiki/%E5%85%B1%E9%80%9A%E9%83%A8%E5%88%86_(%E6%95%B0%E5%AD%A6)
    set1 = {"apples", "bananas", "cherries", "oranges"}
    set2 = {"pears", "apples", "oranges", "kiwis"}

    # Create set 3 by combining set1 and set2 with intersection
    set3 = {"apples", "oranges"}
    assert set3 == {"apples", "oranges"}

def test_set_difference():
    # https://ja.wikipedia.org/wiki/%E5%B7%AE%E9%9B%86%E5%90%88
    set1 = {"apples", "bananas", "cherries", "oranges"}
    set2 = {"pears", "apples", "oranges", "kiwis"}

    # Create set 3 by combining set1 and set2 with difference
    set3 = {"pears", "cherries", "kiwis"}
    assert set3 == {"pears", "kiwis", "cherries"}